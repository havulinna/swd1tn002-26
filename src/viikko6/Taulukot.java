package viikko6;

import java.util.Arrays;

public class Taulukot {

    public static void main(String[] args) {
        // Luodaan uusi kolmen arvon pituinen kokonaislukutaulukko
        int[] numerot = new int[3];

        // Taulukon kaikista indekseistä löytyy alkuarvoina 0
        System.out.println(numerot[0]);
        System.out.println(numerot[1]);
        System.out.println(numerot[2]);

        // Luodaan uusi merkkijonotaulukko alkuarvoilla
        String[] paivat = new String[] { "monday", "tuesday", "wednesday", "thursday" };

        // Vaihdetaan sanan "tuesday" tilalle "tiistai"
        paivat[1] = "tiistai";

        // Taulukko voidaan käydä läpi indeksien avulla
        for (int i = 0; i < paivat.length; i++) {
            System.out.println(paivat[i]);
        }

        // Taulukko voidaan käydä myös läpi for-each -silmukalla
        for (String paiva : paivat) {
            System.out.println(paiva);
        }

        // Taulukon järjestäminen
        Arrays.sort(paivat);

        // Taulukon merkkijonoesityksen tulostaminen
        String merkkijono = Arrays.toString(paivat);
        System.out.println(merkkijono);
    }
}
