package viikko6;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class YhteystietoLista {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);

        List<Yhteystieto> yhteystiedot = new ArrayList<>();
        Yhteystieto matti = new Yhteystieto("Matti", "050555");
        Yhteystieto maija = new Yhteystieto("Maija", "040444");
        Yhteystieto adam = new Yhteystieto("Adam", "0123456");

        yhteystiedot.add(matti);
        yhteystiedot.add(maija);
        yhteystiedot.add(adam);

        System.out.println("Anna poistettava yhteystieto: ");
        String hakusana = lukija.nextLine();

        Yhteystieto poistettava = etsiNimella(yhteystiedot, hakusana);

        if (poistettava != null) {
            yhteystiedot.remove(poistettava);
            System.out.println("Yhteystieto poistettiin.");
        } else {
            System.out.println("Yhteystietoa ei löytynyt.");
        }

        System.out.println(yhteystiedot);
    }

    private static Yhteystieto etsiNimella(List<Yhteystieto> yhteystiedot, String nimi) {
        for (Yhteystieto y : yhteystiedot) {
            if (y.getNimi().equals(nimi)) {
                return y;
            }
        }
        return null;
    }
}
