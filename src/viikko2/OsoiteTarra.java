package viikko2;

import java.util.Scanner;

public class OsoiteTarra {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        System.out.print("Anna etu- ja sukunimi: ");

        String etu = lukija.next();
        String suku = lukija.next();

        String etuAlku = etu.substring(0, 1).toUpperCase();
        String sukuAlku = suku.substring(0, 1).toUpperCase();

        String etuLoppu = etu.substring(1).toLowerCase();
        String sukuLoppu = suku.substring(1).toLowerCase();

        etu = etuAlku + etuLoppu;
        suku = sukuAlku + sukuLoppu;

        System.out.println(etu + " " + suku);
    }

}
