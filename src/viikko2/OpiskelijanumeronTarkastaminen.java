package viikko2;

import java.util.Scanner;

public class OpiskelijanumeronTarkastaminen {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        System.out.print("Kirjoita opiskelijanumero: ");

        String opiskelijanumero = lukija.nextLine();

        boolean oikein = opiskelijanumero.matches("a\\d{7}");

        if (oikein) {
            System.out.println("Oikea muoto!");
        } else {
            System.out.println("Väärä muoto!");
        }
    }

}
