package viikko2;

public class Lahtolaskenta {

    public static void main(String[] args) {
        int luku = 100;

        while (luku > 0) {
            int jakojaannos = luku % 3;
            if (jakojaannos != 0) {
                System.out.println(luku);
            }
            luku--;
        }
    }
}
