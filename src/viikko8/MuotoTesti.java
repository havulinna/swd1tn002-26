package viikko8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MuotoTesti {

    public static void main(String[] args) {
        Muoto m = new Ympyra(10.3);
        Ympyra y = new Ympyra(10);
        Kolmio k = new Kolmio(10.1, 2);
        Nelio n = new Nelio(5);

        List<Muoto> muodot = new ArrayList<>();
        muodot.add(m);
        muodot.add(y);
        muodot.add(k);
        muodot.add(n);

        int tulos = m.compareTo(y);

        System.out.println(tulos); // +1
        System.out.println(y.compareTo(m)); // -1

        Collections.sort(muodot);

        for (Muoto muoto : muodot) {
            System.out.println(muoto);
        }
    }
}
