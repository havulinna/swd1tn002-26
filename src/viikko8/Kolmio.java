package viikko8;

public class Kolmio extends Muoto {

    private double pisinSivu, korkeus;

    public Kolmio(double pisinSivu, double korkeus) {
        super("kolmio");
        this.pisinSivu = pisinSivu;
        this.korkeus = korkeus;
    }

    @Override
    public double pintaAla() {
        return pisinSivu * korkeus / 2;
    }

}
