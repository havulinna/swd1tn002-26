package viikko8;

public abstract class Muoto implements Comparable<Muoto> {
    private String nimi;

    public Muoto(String nimi) {
        this.nimi = nimi;
    }

    public abstract double pintaAla();

    @Override
    public int compareTo(Muoto toinen) {

        if (this.pintaAla() > toinen.pintaAla()) {
            return +1;
        } else if (this.pintaAla() < toinen.pintaAla()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return this.nimi + ", pinta-ala: " + this.pintaAla();
    }
}
