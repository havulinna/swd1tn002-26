package viikko8;

public class Ympyra extends Muoto {

    private double sade;

    public Ympyra(double sade) {
        super("ympyrä");
        this.sade = sade;
    }

    public double kaarenPituus() {
        return 2 * Math.PI * sade;
    }

    @Override
    public double pintaAla() {
        return Math.PI * sade * sade;
    }
}
