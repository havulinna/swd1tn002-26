package viikko7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class KirjoitaTiedosto {

    public static void main(String[] args) {
        // Tiedosto tallennetaan Java-projektin juureen:
        Path polku = Paths.get("uusitiedosto.txt");

        try {
            // Kirjoittaa merkkijonot "a" ja "b" omille riveilleen
            Files.write(polku, Arrays.asList("a", "b"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
