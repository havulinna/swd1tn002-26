package viikko7;

import java.util.Arrays;
import java.util.List;

public class Peltipoliisit {

    private static final String List = null;

    public static void main(String[] args) {
        List<String> syotteet = Arrays.asList("SUT-11;93", "REP-11;122", "PON-1;62", "lopeta");

        int i = 0;
        int maksimiNopeus = -1;
        String maksimiRekkari = null;

        while (true) {
            String rekkariJaNopeus = syotteet.get(i); // lukija.nextLine()
            if (rekkariJaNopeus.equals("lopeta")) {
                break;
            } else {
                String[] osat = rekkariJaNopeus.split(";");
                String rekkari = osat[0];
                int nopeus = Integer.parseInt(osat[1]);

                if (nopeus > maksimiNopeus) {
                    maksimiNopeus = nopeus;
                    maksimiRekkari = rekkari;
                }
            }
            i++;
        }

        System.out.println("Maksimi: " + maksimiNopeus + ", " + maksimiRekkari);
    }
}
