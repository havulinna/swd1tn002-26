package viikko7;

/*
 * Esimerkki siitä, miten etunimitilasto-tehtävässä 
 * olevan nimien lukumäärän saa muutettua kokonaisluvuksi
 */
public class NumeronSiivous {

    public static void main(String[] args) {
        String rivi = "Pekka;33 630;mies";
        String[] osat = rivi.split(";");

        String nimi = osat[0];
        String maara = osat[1];

        // Korvaa luvusta kaikki välilyönnit tyhjillä merkeillä:
        maara = maara.replaceAll(" ", "");

        int maaraLukuna = Integer.parseInt(maara);

        System.out.println(maaraLukuna);

    }
}
