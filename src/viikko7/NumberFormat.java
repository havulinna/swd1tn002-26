package viikko7;

import java.util.Scanner;

public class NumberFormat {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);

        System.out.print("Syötä luku: ");

        try {
            String rivi = lukija.nextLine();
            int luku = Integer.parseInt(rivi);

            System.out.println("Luvun neliö on " + (luku * luku));

        } catch (NumberFormatException e) {

            // System.err:iä voidaan käyttää virheiden tulostamiseen:
            System.err.println("Syötit virheellisen luvun :(");
        }

    }
}
