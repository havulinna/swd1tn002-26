package viikko7;

import java.util.Arrays;
import java.util.List;

public class IndexError {

    public static void main(String[] args) {
        List<String> sanat = Arrays.asList("a", "b", "c");

        // Aiheuttaa java.lang.ArrayIndexOutOfBoundsException -virheen:
        System.out.println(sanat.get(4));
    }
}
