package viikko7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class YhteystietoLukija {

    public static void main(String[] args) {
        try {
            Path polku = Paths.get("yhteystiedot.csv");
            List<String> rivit = Files.readAllLines(polku);

            for (String rivi : rivit) {
                String[] osat = rivi.split(",");
                System.out.println(osat[0]);
            }

        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
