package viikko7;

import java.util.Map;
import java.util.TreeMap;

public class Lempinimet {

    public static void main(String[] args) {
        Map<String, String> lempinimet = new TreeMap<String, String>();

        lempinimet.put("Teemu", "The Finnish Flash");
        lempinimet.put("Jari", "Litti");
        lempinimet.put("Kaisa-Leena", "Kappa");

        System.out.println(lempinimet);
    }
}
