package viikko7;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PeltipoliisitMapinAvulla {

    public static void main(String[] args) {
        List<String> syotteet = Arrays.asList("SUT-11;93", "REP-11;122", "PON-1;62", "lopeta");
        Map<String, Integer> tulokset = new HashMap<>();

        int summa = 0;
        int i = 0;
        while (true) {
            String rekkariJaNopeus = syotteet.get(i); // lukija.nextLine()
            if (rekkariJaNopeus.equals("lopeta")) {
                break;
            } else {
                String[] osat = rekkariJaNopeus.split(";");
                String rekkari = osat[0];
                int nopeus = Integer.parseInt(osat[1]);

                tulokset.put(rekkari, nopeus);
                summa += nopeus;
            }
            i++;
        }

        Set<Entry<String, Integer>> parit = tulokset.entrySet();

        int maksimiNopeus = -1;
        String maksimiRekkari = null;

        Integer minimiNopeus = null;
        String minimiRekkari = null;

        for (Entry<String, Integer> rekkariJaNopeus : parit) {
            int parinNopeus = rekkariJaNopeus.getValue();
            if (parinNopeus > maksimiNopeus) {
                maksimiNopeus = parinNopeus;
                maksimiRekkari = rekkariJaNopeus.getKey();
            }
            if (minimiNopeus == null || parinNopeus < minimiNopeus) {
                // ...
            }
        }
        System.out.println("Maksimi: " + maksimiNopeus + ", " + maksimiRekkari);

        // Keskiarvo tapa 1: kerätään summa muuttujaan
        System.out.println(1.0 * summa / i);

        // Keskiarvo tapa 2: käydään mapista arvot läpi
        int arvojenSumma = 0;
        for (int tallennettuNopeus : tulokset.values()) {
            arvojenSumma += tallennettuNopeus;
        }

        System.out.println(1.0 * arvojenSumma / tulokset.size());

        System.out.println(tulokset);

    }
}
