package viikko7;

public class PrintExample {

    public static void main(String[] args) {
        // Kun suoritat ohjelman, tulostuvat luvut todennäköisesti
        // aivan toisessa järjestyksessä, koska out- ja err-tietovirrat
        // puskuroivat tulostettavaa dataa.
        System.out.println("1");
        System.err.println("2");
        System.out.println("3");
        System.err.println("4");
        System.out.println("5");
        System.err.println("6");
    }
}
