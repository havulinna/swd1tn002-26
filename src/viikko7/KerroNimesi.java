package viikko7;

import java.util.Scanner;

public class KerroNimesi {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);

        System.out.print("Kirjoita nimesi:");
        String nimi = lukija.nextLine();

        if (nimi.length() == 0 || nimi.length() > 100) {
            throw new IllegalArgumentException("Virheellinen nimi!");
        }

        System.out.println("Kiitos!");
        lukija.close();
    }
}
