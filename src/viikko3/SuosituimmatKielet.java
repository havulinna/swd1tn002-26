package viikko3;

import java.util.ArrayList;
import java.util.List;

public class SuosituimmatKielet {

    public static void main(String[] args) {
        List<String> kielet = new ArrayList<String>();

        kielet.add("JavaScript");
        kielet.add("Python");
        kielet.add("Java");
        kielet.add("PHP");
        kielet.add("Ruby");

        System.out.println(kielet.subList(2, 4));

    }
}
