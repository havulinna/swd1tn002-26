package viikko3;

public class NumeroVertailu {

    public static void main(String[] args) {

        Integer a = new Integer(100);
        Integer b = new Integer(100);

        // Tulostaa false, koska a ja b ovat eri objektit
        System.out.println(a == b);
    }
}
