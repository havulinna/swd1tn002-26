package viikko3;

import java.util.Scanner;

public class Laskin {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);

        System.out.print("Anna luku 1: ");
        int a = Integer.parseInt(lukija.nextLine());

        System.out.print("Anna luku 2: ");
        int b = Integer.parseInt(lukija.nextLine());

        System.out.print("Anna operaatio: ");
        String op = lukija.nextLine();

        System.out.println(a + " " + op + " " + b);
    }
}
