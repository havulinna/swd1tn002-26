package viikko5;

public class Esimerkki {

    @SuppressWarnings("unused")
    public static void main(String[] args) {

        Yhteystieto y1 = new Yhteystieto("Adam", "050123", "adam@example.com");
        Yhteystieto y2 = y1;

        // Sekä y1 että y2 viittaavat samaan objektiin, joten tämä muutos
        // näkyy molempien muuttujien kautta:
        y1.setEmail("a@example.com");

        System.out.println(y1.getEmail());
        System.out.println(y2.getEmail());
    }
}
