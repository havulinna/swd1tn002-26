package viikko5;

import java.util.ArrayList;
import java.util.List;

public class Puhelinluettelo {

    private List<Yhteystieto> yhteystiedot;

    public Puhelinluettelo() {
        this.yhteystiedot = new ArrayList<Yhteystieto>();
    }

    public void lisaaYhteystieto(Yhteystieto y) {
        this.yhteystiedot.add(y);
    }

    public Yhteystieto haeYhteystieto(int indeksi) {
        if (indeksi < this.yhteystiedot.size()) {
            return this.yhteystiedot.get(indeksi);
        } else {
            return null;
        }
    }
}
