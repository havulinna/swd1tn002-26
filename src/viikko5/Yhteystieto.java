package viikko5;

public class Yhteystieto {
    private String nimi;
    private String email;
    private String puhelin;

    public Yhteystieto() {

    }

    public Yhteystieto(String nimi, String email, String puhelin) {
        this.nimi = nimi;
        this.email = email;
        this.puhelin = puhelin;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public boolean asetaEmail(String e) {
        if (e.matches(".+@.+\\..+")) {
            this.email = e;
            return true;
        } else {
            return false;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPuhelin() {
        return puhelin;
    }

    public void setPuhelin(String puhelin) {
        this.puhelin = puhelin;
    }

    @Override
    public String toString() {
        return this.nimi + ", " + this.email + ", " + this.puhelin;
    }

    public void tulostaNimi() {
        System.out.println(this.nimi);
    }

    public String kerroEmail() {
        return this.email;
    }

}