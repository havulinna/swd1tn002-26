package viikko4;

import java.util.Scanner;

public class SuoKuokkaJaJava {
    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        System.out.print("Kuinka monta kertaa tulostetaan? ");
        int kertoja = lukija.nextInt();

        System.out.println();
        for (int i = 0; i < kertoja; i++) {
            tulostaTeksti();
        }
        lukija.close();
    }

    public static void tulostaTeksti() {
        System.out.println("Alussa olivat suo, kuokka ja Java.");
    }
}