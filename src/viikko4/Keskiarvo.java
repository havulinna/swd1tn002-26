package viikko4;

public class Keskiarvo {

    public static void main(String[] args) {
        int luku1 = 4;
        int luku2 = 7;
        int luku3 = 11;

        double keskiarvo = keskiarvo(luku1, luku2, luku3);
        System.out.println("Keskiarvo on: " + keskiarvo);
    }

    public static double keskiarvo(int a, int b, int c) {
        int summa = a + b + c;
        double ka = summa / 3.0;
        return ka;
    }
}
