package viikko4;

import java.util.Scanner;

public class SuoKuokkaJaJava2 {
    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        System.out.print("Kuinka monta kertaa tulostetaan? ");
        int kertoja = lukija.nextInt();
        tulostaTeksti(kertoja);
    }

    public static void tulostaTeksti(int kertaa) {
        for (int i = 0; i < kertaa; i++) {
            System.out.println("Alussa olivat suo, kuokka ja Java.");
        }
    }
}