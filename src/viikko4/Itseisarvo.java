package viikko4;

public class Itseisarvo {

    public static void main(String[] args) {
        double tulos = itseisarvo(-8.0);

        System.out.println(tulos);
    }

    private static double itseisarvo(double luku) {
        if (luku > 0) {
            return luku;
        } else {
            return luku * -1;
        }
    }
}
