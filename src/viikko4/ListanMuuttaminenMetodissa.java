package viikko4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListanMuuttaminenMetodissa {

    public static void main(String[] args) {
        List<String> sanat = new ArrayList<>();
        sanat.add("appelsiini");
        sanat.add("omena");
        sanat.add("banaani");

        System.out.println("Lista alussa: " + sanat);

        tulostaAakkosJarjestyksessa(sanat);

        System.out.println("Lista lopussa: " + sanat);
    }

    private static void tulostaAakkosJarjestyksessa(List<String> sanat) {
        sanat = new ArrayList<String>(sanat);
        Collections.sort(sanat);
        System.out.println(sanat);
    }
}
