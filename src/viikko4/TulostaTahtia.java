package viikko4;

public class TulostaTahtia {
    public static void main(String[] args) {
        int alku = 1;
        int loppu = 5;

        tulostaTahtia(alku, loppu);
    }

    public static void tulostaTahtia(int alku, int loppu) {
        while (alku < loppu) {
            System.out.print("*");
            alku++;
        }
    }
}
