package viikko4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MinigolfTilasto {

    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        System.out.print("Anna pelaajien lukumäärä: ");
        int lukumaara = lukija.nextInt();
        List<Integer> tulokset = new ArrayList<>();

        for (int i = 1; i <= lukumaara; i++) {
            System.out.println("Anna pelaajan " + i + ". tulos:");
            int tulos = lukija.nextInt();
        }
    }
}
